package my.net.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.alibaba.fastjson.JSON;

public class Message implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final int version = 1;

	private String cmd = "";
	
	private boolean ok=true;
	
	private Object data;
	
	private String errorMsg;

	public String getCmd() {
		return cmd;
	}

	public Message setCmd(String cmd) {
		this.cmd = cmd;
		return this;
	}

	public Object getData() {
		return data;
	}

	public Message setData(Object data) {
		this.data = data;
		return this;
	}

	public boolean isOk() {
		return ok;
	}

	public Message setOk(boolean ok) {
		this.ok = ok;
		return this;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}

	public Message setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
		return this;
	}

	public String toJson() {
		return JSON.toJSONString(this);
	}

	public <T> T getData(Class<T> clazz) {
		if (data == null)
			return null;

		if (data instanceof JSON) {
			return JSON.toJavaObject((JSON) data, clazz);
		}
		if (data instanceof String) {
			return JSON.parseObject(data.toString(), clazz);
		}
		return null;
	}

	public static Message build(String json) {
		return JSON.parseObject(json, Message.class);
	}

	public int getVersion() {
		return version;
	}

	@Override
	public String toString() {

		return ToStringBuilder.reflectionToString(this);
	}

	public static void main(String[] args) {
		System.out.println(new Message());
	}

}
