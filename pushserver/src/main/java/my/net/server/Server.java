package my.net.server;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import my.net.bean.Message;
import my.net.server.service.PushServer;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

/**
 * The Push Server
 */
public class Server {

    private final int port;

    public Server(int port) {
        this.port = port;
    }

    public void run() {
    	PushServer.register("my.net.server.service");  
    	
    	 Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(new Runnable() {
 			
 			@Override
 			public void run() {
 				Message msg = new Message();
 				msg.setCmd("tips");
 				msg.setData("有新消息了");
 				System.out.println("broadcast");
 				System.out.println(msg.toJson());
 				PushServer.broadcast(msg);
 			}
 		}, 1, 10, TimeUnit.MINUTES);
    	
        // Configure the server.
    	ServerBootstrap bootstrap = new ServerBootstrap(
    			new NioServerSocketChannelFactory(
    					Executors.newCachedThreadPool(),
    					Executors.newCachedThreadPool()));

    	// Configure the pipeline factory.
    	bootstrap.setPipelineFactory(new ServerPipelineFactory());
    	
    	bootstrap.setOption("child.tcpNoDelay", true);
        bootstrap.setOption("child.keepAlive", true);

        // Bind and start to accept incoming connections.
        bootstrap.bind(new InetSocketAddress(port));
        
       
    }

    public static void main(String[] args) throws Exception {
        new Server(7777).run();
    }
}  