package my.net.server.service;

import my.net.bean.Message;

import org.jboss.netty.channel.Channel;

public interface Processor {
	public String name();
	public boolean needLogin();
	public Message process(Message msg,Channel channel);
}
