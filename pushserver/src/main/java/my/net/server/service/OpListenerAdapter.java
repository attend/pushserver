package my.net.server.service;

import org.slf4j.Logger;


public class OpListenerAdapter implements OpListener{
	protected Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass()	);
	@Override
	public void onOk(String userid) {
		logger.debug("onOk,userid:{}",userid);
		
	}

	@Override
	public void onError(String userid) {
		logger.debug("onError,userid:{}",userid);
		
	}

	@Override
	public void onCancel(String userid) {
		logger.debug("onCancel,userid:{}",userid);
	}
}
