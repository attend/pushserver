package my.net.server.service;

public interface OpListener {
		public void onOk(String userid);
		public void onError(String userid);
		public void onCancel(String userid);
}
