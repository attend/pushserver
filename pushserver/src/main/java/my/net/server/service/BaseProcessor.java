package my.net.server.service;

import my.net.bean.Message;

import org.jboss.netty.channel.Channel;

public abstract class BaseProcessor implements Processor{

	@Override
	public boolean needLogin() {
		return false;
	}

	@Override
	public Message process(Message msg, Channel channel) {
		try{
			return  doProcess(msg, channel);
		}catch(Exception e){
			//业务错误
			Message ret = new Message();
			ret.setOk(false);
			ret.setErrorMsg(e.getMessage());
			return ret;
		}
	}
	
	public abstract Message doProcess(Message msg,Channel channel);

}
