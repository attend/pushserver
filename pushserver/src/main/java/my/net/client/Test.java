package my.net.client;

import java.io.IOException;
import java.net.UnknownHostException;

import my.net.bean.Message;
import my.net.bean.User;

public class Test {

	public static void main(String[] args) throws UnknownHostException,
			IOException {
		
		for(int i=0;i<1;i++){
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						test();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			
			t.start();
		}
		
	}

	private static void test() throws IOException {
		final TSocket socket = new TSocket("localhost", 9999);

		socket.open();

		Message msg = new Message();
		msg.setCmd("login");
		User user = new User();
		user.setUserid("test");
		user.setPassword("p");

		msg.setData(user);

		for (int i = 0; i < 2; i++) {

			socket.write(msg);

		}

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Message msg = socket.read();
						System.out.println(msg);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					}
				}
			}
		});

		t.start();
	}
}
