package my.util;


/**
 * 
 * @author linan
 * 
 */
public class ClassUtil {
	/*
	 * 判断一个类是否实现了某个借口
	 */
	public static boolean isInterface(Class<?> cls, Class<?> interfaceCls) {
		if (cls.isInterface())
			return false;
		for (Class<?> c : cls.getInterfaces()) {
			if (c == interfaceCls) {
				return true;
			}
		}
		if (cls.getSuperclass() != Object.class) {
			return isInterface(cls.getSuperclass(), interfaceCls);
		}
		return false;
	}
}
