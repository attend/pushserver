package my.util;

import java.io.File;
import java.io.FileFilter;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PackageScan {

	private static Logger logger = LoggerFactory.getLogger(PackageScan.class);

	/**
	 * 从package及子package中获得Class，忽略掉内部类
	 * 
	 * @param packageName
	 * @return
	 */
	public static Set<Class<?>> getClasses(String packageName) {
		return getClasses(packageName, null);
	}

	/**
	 * 从package及子package中获得Class，忽略掉内部类
	 * 
	 * @param packageName
	 *            包名 如com.test
	 * @param filter
	 * @return
	 */
	public static Set<Class<?>> getClasses(String packageName,
			ClassFilter filter) {

		// 第一个class类的集合
		Set<Class<?>> classes = new LinkedHashSet<Class<?>>();

		// 是否循环迭代
		boolean recursive = true;
		// 获取包的名字 并进行替换
		String packageDirName = packageName.replace('.', '/');
		// 定义一个枚举的集合 并进行循环来处理这个目录下的things

		Enumeration<URL> dirs;
		try {
			dirs = Thread.currentThread().getContextClassLoader()
					.getResources(packageDirName);
			// 循环迭代下去
			while (dirs.hasMoreElements()) {
				// 获取下一个元素
				URL url = dirs.nextElement();
				// 得到协议的名称
				String protocol = url.getProtocol();
				// 如果是以文件的形式保存在服务器上
				if ("file".equals(protocol)) {
					// 获取包的物理路径
					String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
					logger.info("scan from file {}"+filePath);
					// 以文件的方式扫描整个包下的文件 并添加到集合中
					findAndAddClassesInPackageByFile(packageName, filePath,
							recursive, classes, filter);
				} else if ("jar".equals(protocol)) {
					// 如果是jar包文件
					// 定义一个JarFile
					logger.info("scan from jar");
					JarFile jar;
					try {
						// 获取jar
						jar = ((JarURLConnection) url.openConnection())
								.getJarFile();
						// 从此jar包 得到一个枚举类
						Enumeration<JarEntry> entries = jar.entries();
						// 同样的进行循环迭代
						while (entries.hasMoreElements()) {
							// 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
							JarEntry entry = entries.nextElement();
							String name = entry.getName();
							// 如果是以/开头的
							if (name.charAt(0) == '/') {
								// 获取后面的字符串
								name = name.substring(1);
							}
							// 如果前半部分和定义的包名相同
							if (name.startsWith(packageDirName)) {
								int idx = name.lastIndexOf('/');
								// 如果以"/"结尾 是一个包
								if (idx != -1) {
									// 获取包名 把"/"替换成"."
									packageName = name.substring(0, idx)
											.replace('/', '.');
								}
								// 如果可以迭代下去 并且是一个包
								if ((idx != -1) || recursive) {
									// 如果是一个.class文件 而且不是目录
									if (name.endsWith(".class")
											&& !entry.isDirectory()) {
										// 去掉后面的".class" 获取真正的类名
										String className = name.substring(
												packageName.length() + 1,
												name.length() - 6);
										try {

											loadClass(classes, packageName,
													className, filter);
										} catch (Exception e) {
											logger.error("error", e);

										} catch (Error e) {
											logger.error("error", e);
										}
									}
								}
							}
						}
					} catch (Exception e) {

						logger.error("error", e);
					}
				}
			}
		} catch (Exception e) {
			logger.error("error", e);
		}

		return classes;
	}

	private static void loadClass(Set<Class<?>> classes, String packageName,
			String className, ClassFilter filter) throws ClassNotFoundException {
		// 忽略掉内部类
		if (className.indexOf("$") > 0) {
			return;
		}
		Class<?> loadClass = Thread.currentThread().getContextClassLoader()
				.loadClass(packageName + '.' + className);
		if (filter == null || filter.accept(loadClass)) {
			logger.debug("find class {}", loadClass.getName());
			classes.add(loadClass);
		}

	}

	private static void findAndAddClassesInPackageByFile(String packageName,
			String packagePath, final boolean recursive, Set<Class<?>> classes,
			ClassFilter filter) {
		File dir = new File(packagePath);
		if (!dir.exists() || !dir.isDirectory()) {
			return;
		}
		File[] files = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return (recursive && file.isDirectory())
						|| (file.getName().endsWith(".class"));
			}
		});
		for (File file : files) {
			if (file.isDirectory()) {
				findAndAddClassesInPackageByFile(
						packageName + "." + file.getName(),
						file.getAbsolutePath(), recursive, classes, filter);
			} else {
				String className = file.getName().substring(0,
						file.getName().length() - 6);
				try {
					loadClass(classes, packageName, className, filter);
				} catch (Exception e) {
					logger.error("error", e);
				} catch (Error e) {
					logger.error("error", e);
				}
			}
		}
	}

	public static void main(String[] args) {
		for (Class<?> cls : getClasses("org")) {
			System.out.println(cls.getName());
		}
	}
}
