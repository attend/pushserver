package my.util;

public interface ClassFilter {
	public boolean accept(Class<?> cls);
}
