package my.demo;

public class User {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String yhzh;//账号
	private String yhmm;//密码
	private String xtsb;//系统识别
	private String ks;//登录科室
	
	public String getYhzh() {
		return yhzh;
	}
	public void setYhzh(String yhzh) {
		this.yhzh = yhzh;
	}
	public String getYhmm() {
		return yhmm;
	}
	public void setYhmm(String yhmm) {
		this.yhmm = yhmm;
	}
	public String getXtsb() {
		return xtsb;
	}
	public void setXtsb(String xtsb) {
		this.xtsb = xtsb;
	}
	public String getKs() {
		return ks;
	}
	public void setKs(String ks) {
		this.ks = ks;
	}
	
	
	
}
